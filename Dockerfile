FROM registry.gitlab.com/python-gitlab/python-gitlab:slim-bullseye

COPY . /app

RUN pip install -r /app/requirements.txt

WORKDIR /app
ENV PYTHONPATH '/app/'
