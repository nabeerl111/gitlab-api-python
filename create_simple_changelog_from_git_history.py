#!/usr/bin/env python

# Description: Show how the Git history of a project can be fetched and printed. No support for releases/tags, or filtering merge commits to keep it simple.
# Requirements: python-gitlab Python libraries. GitLab API read access, and maintainer access to all configured groups/projects.
# Author: Michael Friedrich <mfriedrich@gitlab.com>
# License: MIT, (c) 2023-present GitLab B.V.

import gitlab
import os
import sys

GITLAB_SERVER = os.environ.get('GL_SERVER', 'https://gitlab.com')
# https://gitlab.com/everyonecancontribute/observability/o11y.love
PROJECT_ID = os.environ.get('GL_PROJECT_ID', 32738951)
GITLAB_TOKEN = os.environ.get('GL_TOKEN')

if not GITLAB_TOKEN:
    print("Please set the GL_TOKEN env variable.")
    sys.exit(1)

gl = gitlab.Gitlab(GITLAB_SERVER, private_token=GITLAB_TOKEN)

# Main
project = gl.projects.get(PROJECT_ID)
commits = project.commits.list(ref_name='main', lazy=True, iterator=True)

print("# Changelog")

for commit in commits:
    # Generate a markdown formatted list with URLs
    print("- [{text}]({url}) ({name})".format(text=commit.title, url=commit.web_url, name=commit.author_name))

    # debug
    #print(commit.diff())

# Exercise: Only list the history for a specific file
# https://stackoverflow.com/questions/70773075/how-to-get-gitlab-commits-of-a-file-using-python-gitlab-module


