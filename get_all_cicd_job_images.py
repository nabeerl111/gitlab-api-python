#!/usr/bin/env python

# Description: Fetch projects (all, group, project), get the merged linted CI/CD configuration, and print the image string
# Requirements: python-gitlab Python libraries. GitLab API read access, and maintainer access to all configured groups/projects.
# Author: Michael Friedrich <mfriedrich@gitlab.com>
# License: MIT, (c) 2023-present GitLab B.V.

import gitlab
import os
import sys
import yaml

GITLAB_SERVER = os.environ.get('GL_SERVER', 'https://gitlab.com')
GITLAB_TOKEN = os.environ.get('GL_TOKEN') # token requires developer permissions
PROJECT_ID = os.environ.get('GL_PROJECT_ID') #optional
# https://gitlab.com/gitlab-de/use-cases/docker
GROUP_ID = os.environ.get('GL_GROUP_ID', 65096153) #optional

#################
# Main

if __name__ == "__main__":
    if not GITLAB_TOKEN:
        print("🤔 Please set the GL_TOKEN env variable.")
        sys.exit(1)

    gl = gitlab.Gitlab(GITLAB_SERVER, private_token=GITLAB_TOKEN)

    # Collect all projects, or prefer projects from a group id, or a project id
    projects = []

    # Direct project ID
    if PROJECT_ID:
        projects.append(gl.projects.get(PROJECT_ID))

    # Groups and projects inside
    elif GROUP_ID:
        group = gl.groups.get(GROUP_ID)

        for project in group.projects.list(include_subgroups=True, all=True):
            # https://python-gitlab.readthedocs.io/en/stable/gl_objects/groups.html#examples
            manageable_project = gl.projects.get(project.id)
            projects.append(manageable_project)

    # All projects on the instance (may take a while to process)
    else:
        projects = gl.projects.list(get_all=True)

    print("# Summary of projects and their CI/CD image usage")

    # Loop over projects, fetch .gitlab-ci.yml, run the linter to get the full translated config, and extract the `image:` setting
    for project in projects:

        print("# Project: {name}, ID: {id}\n\n".format(name=project.name_with_namespace, id=project.id))

        # https://python-gitlab.readthedocs.io/en/stable/gl_objects/projects.html#project-files - not needed but helpful for custom path search
        #data = project.files.raw(file_path='.gitlab-ci.yml', ref='main') # TODO: fetch default branch dynamically

        # https://python-gitlab.readthedocs.io/en/stable/gl_objects/ci_lint.html
        lint_result = project.ci_lint.get()
        # print(lint_result.merged_yaml) # this is yaml

        data = yaml.safe_load(lint_result.merged_yaml)
        #print(data) #debug

        for d in data:
            #print(d) #debug
            print("Job name: {n}".format(n=d))
            for attr in data[d]:
                if 'image' in attr:
                    print("Image: {i}".format(i=data[d][attr]))

        print("\n\n")

        # Loop through all jobs (this can be expensive, and is not working well yet. Remove the continue statement below to play).
        continue

        for job in project.jobs.list():
            log_trace = str(job.trace())

            print(log_trace)

            if 'image' in log_trace:
                print("Job ID: {i}, URL {u}".format(i=job.id, u=job.web_url))
                print(log_trace)


sys.exit(0)