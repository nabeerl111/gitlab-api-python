#!/usr/bin/env python

# Description: Fetch all branches from a given project ID, and showcase different merge state filters.
# Requirements: python-gitlab Python libraries. GitLab API read access, and maintainer access to all configured groups/projects.
# Author: Michael Friedrich <mfriedrich@gitlab.com>
# License: MIT, (c) 2023-present GitLab B.V.

import gitlab
import os
import sys

GITLAB_SERVER = os.environ.get('GL_SERVER', 'https://gitlab.com')
# https://gitlab.com/dnsmichi/opsindev.news
PROJECT_ID = os.environ.get('GL_PROJECT_ID', 33298437)
GITLAB_TOKEN = os.environ.get('GL_TOKEN')

if not GITLAB_TOKEN:
    print("Please set the GL_TOKEN env variable.")
    sys.exit(1)

gl = gitlab.Gitlab(GITLAB_SERVER, private_token=GITLAB_TOKEN)

# Main
project = gl.projects.get(PROJECT_ID, lazy=False, pagination="keyset", order_by="updated_at", per_page=100)

# Get all branches
real_branches = []
for branch in project.branches.list():
    real_branches.append(branch)
    # print(branch) # debug

print("All branches")
for rb in real_branches:
    print("Branch: {b}".format(b=rb.name))

# Get all merged branches
merged_branches_names = []
for branch in real_branches:
    if branch.default:
        continue # ignore the default branch for merge status

    if branch.merged:
        merged_branches_names.append(branch.name)

print("Branches merged: {b}".format(b=", ".join(merged_branches_names)))

# Get un-merged branches
not_merged_branches_names = []
for branch in real_branches:
    if branch.default:
        continue # ignore the default branch for merge status

    if not branch.merged:
        not_merged_branches_names.append(branch.name)

print("Branches not merged: {b}".format(b=", ".join(not_merged_branches_names)))