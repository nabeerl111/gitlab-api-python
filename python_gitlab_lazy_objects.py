#!/usr/bin/env python

# Description: Show how lazy load with objects works in python-gitlab
# Requirements: python-gitlab Python libraries. GitLab API read access, and maintainer access to all configured groups/projects.
# Author: Michael Friedrich <mfriedrich@gitlab.com>
# License: MIT, (c) 2023-present GitLab B.V.

import gitlab
import os
import sys

GITLAB_SERVER = os.environ.get('GL_SERVER', 'https://gitlab.com')
# https://gitlab.com/gitlab-de/use-cases/gitlab-api/gitlab-api-python
PROJECT_ID = os.environ.get('GL_GROUP_ID', 42491852)
GITLAB_TOKEN = os.environ.get('GL_TOKEN')

if not GITLAB_TOKEN:
    print("Please set the GL_TOKEN env variable.")
    sys.exit(1)

gl = gitlab.Gitlab(GITLAB_SERVER, private_token=GITLAB_TOKEN)

# Lazy object, no API call
project = gl.projects.get(PROJECT_ID, lazy=True)

try:
    print("Trying to access 'snippets_enabled' on a lazy loaded project object. This will throw an exception that we capture.")
    print("Project settings: snippets_enabled={b}".format(b=project.snippets_enabled))
except Exception as e:
    print("Accessing lazy loaded object failed: {e}".format(e=e))

project.snippets_enabled = True

project.save() # This creates an API call

print("\nLazy object was loaded after save() call.")
print("Project settings: snippets_enabled={b}".format(b=project.snippets_enabled))



